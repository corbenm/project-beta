from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.CharField(max_length=200)

class Technician(models.Model):
    first_name = (models.CharField(max_length=200))
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    customer = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default="PENDING")
    vin = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,

    )
