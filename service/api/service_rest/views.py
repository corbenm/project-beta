from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# Create your views here.
class technicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]
class smallTechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name"
    ]
class appointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer",
        "date_time",
        "reason",
        "status",
        "vin",
        "technician"
    ]
    encoders = {
        "technician": smallTechnicianEncoder()
    }

class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=technicianListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                technicians,
                encoder=technicianListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def list_automobileVOs(request):
    autos = AutomobileVO.objects.all()
    return JsonResponse(
        {"autos": autos},
        encoder=AutomobileVoEncoder,
    )

@require_http_methods(["DELETE"])
def delete_technician(request, pk):
    try:
        count, _ = Technician.objects.get(id=pk).delete()
        return JsonResponse(({"deleted": count > 0}))
    except Technician.DoesNotExist:
        response = JsonResponse({"Error": "Technician does not exist"})
        response.status_code = 400
        return response

@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=appointmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid employee id"}
            )
            response.status_code = 400
            return response
        try:
            appointments = Appointment.objects.create(**content)
            return JsonResponse(
                appointments,
                encoder=appointmentListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add an appointment"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def delete_appointment(request, pk):
    try:
        count, _ = Appointment.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    except Appointment.DoesNotExist:
        response = JsonResponse({"Error": "Appointment does not exist"})
        response.status_code = 400
        return response

@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "CANCELLED"
        appointment.save()
        return JsonResponse(
            {"Updated": appointment},
            encoder=appointmentListEncoder,
            safe=False,
            )
    except Appointment.DoesNotExist:
        response = JsonResponse({"Error": "Appointment does not exist"})
        response.status_code = 400
        return response

@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "FINISHED"
        appointment.save()
        return JsonResponse(
            {"Updated": appointment},
            encoder=appointmentListEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"Error": "Appointment does not exist"})
        response.status_code = 400
        return response
