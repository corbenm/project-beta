// import React, {useState} from "react";

import { useState } from "react";

function AppointmentForm(props){
const [customer, setCustomer] = useState('');
const [vinNumber, setVin] = useState('');
const [date, setDate] = useState('');
const [time, setTime] = useState('');
const [reason, setReason] = useState('');
const [technician, setTechnician] = useState('');

const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
}
const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
}
const handleDateChange = (event) => {
    const value = event.target.value;
    setDate(value);
}
const handleTimeChange = (event) => {
    const value = event.target.value;
    setTime(value);
}
const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
}
const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
}

async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.customer = customer;
    data.date_time = (date + ' ' + time);
    data.reason = reason;
    data.vin = vinNumber;
    data.technician = technician;

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchconfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(appointmentUrl, fetchconfig);
    if (response.ok){
        const newAppointment = await response.json();
        console.log(newAppointment);
        setCustomer('');
        setVin('');
        setDate('');
        setTime('');
        setReason('');
        setTechnician('');
        props.getAppointments();
    }
}


return (
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Create a service appointment</h1>
  <form onSubmit={handleSubmit} id="create-appointment-form">
  <div className="form-floating mb-3">
      <input placeholder="Customer" onChange={handleCustomerChange} value={customer} required type="text" name="customer" id="customer" className="form-control" />
      <label htmlFor="customer">Customer</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="VIN Number" onChange={handleVinChange} value={vinNumber} required type="text" name="vin" id="Vin" className="form-control" />
      <label htmlFor="vin">VIN Number</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Date" onChange={handleDateChange} value={date} required type="date" name="date" id="date" className="form-control" />
      <label htmlFor="date">Date</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Time" onChange={handleTimeChange} value={time} required type="time" name="time" id="time" className="form-control" />
      <label htmlFor="time">Time</label>
    </div>
    <div className="form-floating mb-3">
      <textarea placeholder="Reason" onChange={handleReasonChange} value={reason} className="form-control" id="reason"></textarea>
      <label htmlFor="reason">Reason</label>
    </div>
    <div className="mb-3">
      <select id="Technician" onChange={handleTechnicianChange} value={technician} required name="technician" className="form-select" >
      <option value="">Choose a technician</option>
            {props.technicians.map(technician => {
            return (
            <option key={technician.id} value={technician.id}> {technician.first_name} {technician.last_name} </option>
            );
            })};
      </select>
    </div>
    <button className="btn btn-primary">Book Appointment</button>
  </form>
</div>
</div>
</div>
    )
}
export default AppointmentForm
