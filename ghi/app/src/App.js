import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import {useState, useEffect} from 'react';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import ManufacurerList from './ManufactureList';
import ManufactureForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import VehicleList from './VehicleList';
import VehicleForm from './VehicleForm';
import CustomerForm from "./CustomerForm";
import CustomerList from "./CustomerList";
import SalespeopleList from './SalespeopleList';
import SalespeopleForm from './SalespeopleForm';


function App() {
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [autos, setAutos] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salespeople, setSalesPeople] = useState([]);
  async function getTechnicians() {
    const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const { technicians } = await response.json();
      setTechnicians(technicians);
    } else {
      console.log('An error occurred while getting technicians');
    }
  }

  async function getAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const { appointments } = await response.json();
      setAppointments(appointments);
    } else {
      console.log('An error has occurred while getting appointments')
    }
  }

  async function getManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const { manufacturers } = await response.json();
      setManufacturers(manufacturers);
    } else {
      console.log('An error occurred while fetching manufacturers')
    }
  }

  async function getModels() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const { models } = await response.json();
      setModels(models);
    } else {
      console.log('An error occurred while getting models')
    }
  }

  async function getAutos() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
        const {autos} = await response.json();
        setAutos(autos);
    } else {
        console.log('There was an issue fetching the automobiles');
    }
}
async function getCustomers() {
  const response = await fetch('http://localhost:8090/api/customers/');
  if (response.ok) {
    const { customers } = await response.json();
    setCustomers(customers);
  } else {
    console.log('An error has occurred while getting your customers')
  }
}
async function getSalespeople() {
  const response = await fetch('http://localhost:8090/api/salespeople/');
  if (response.ok) {
    const { salesperson } = await response.json();
    setSalesPeople(salesperson);
  } else {
    console.log('An error has occurred while getting Sales People')
  }
}



  useEffect(() => {
    getTechnicians();
    getAppointments();
    getManufacturers();
    getModels();
    getAutos();
    getCustomers();
    getSalespeople();
  }, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/">
            <Route path="" element={<MainPage />} />
          </Route>
          <Route path="manufacturer/">
            <Route path ="list" element={<ManufacurerList manufacturers={manufacturers} />} />
            <Route path="new" element={<ManufactureForm getManufacturers={getManufacturers} />} />
          </Route>
          <Route path="model/">
            <Route path="list" element={<ModelList models={models} />} />
            <Route path="new" element={<ModelForm getModels={getModels} manufacturers={manufacturers} />} />
          </Route>
          <Route path="vehicle/">
            <Route path="list" element={<VehicleList autos={autos} />} />
            <Route path="new" element={< VehicleForm getAutos={getAutos} models={models} />} />
          </Route>
          <Route path="technician/">
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians} />} />
            <Route path="list" element={<TechnicianList technicians={technicians} />} />
          </Route>
          <Route path="appointment/">
            <Route path="new" element={<AppointmentForm technicians={technicians} getAppointments={getAppointments} />} />
            <Route path="list" element={<AppointmentList appointments={appointments} getAppointments={getAppointments} />} />
            <Route path="history" element={<ServiceHistory appointments={appointments} />} />
          </Route>
          <Route path="customer/">
            <Route path="new" element={<CustomerForm getCustomers={getCustomers} />} />
            <Route path="list" element={<CustomerList customers={customers}/>}/>
          </Route>
          <Route path="salesperson/">
            <Route path="new" element={<SalespeopleForm getSalespeople={getSalespeople} />} />
            <Route path="list" element={<SalespeopleList salespeople={salespeople}/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
