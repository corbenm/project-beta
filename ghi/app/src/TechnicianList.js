
function TechnicianList(props){
    return(
        <>
        <h1>Technicians</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee Id</th>
                </tr>
            </thead>
            <tbody>
                {props.technicians.map(technician => {
                    return (
                        <tr key={technician.id}>
                            <td>{ technician.first_name }</td>
                            <td>{ technician.last_name }</td>
                            <td>{ technician.employee_id }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}
export default TechnicianList;
