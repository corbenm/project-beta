import { useState } from "react";
function CustomerForm(props){
const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [address, setAddress] = useState('');
const [phoneNumber, setPhoneNumber] = useState('');

const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
}
const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
}
const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
}
const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
}

async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.first_name = firstName;
    data.last_name = lastName;
    data.address = address;
    data.phone_number = phoneNumber;

    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchconfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(customerUrl, fetchconfig);
    if (response.ok) {
        const newCustomer = await response.json();
        console.log(newCustomer);
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhoneNumber('');
        props.getCustomers();
    }
}
    return(
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Add a Customer</h1>
  <form id="create-customer-form" onSubmit={handleSubmit}>
    <div className="form-floating mb-3">
      <input placeholder="First name" onChange={handleFirstNameChange} value={firstName} required type="text" name="first_name" id="first_name" className="form-control" />
      <label htmlFor="first name">First name</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Last name" onChange={handleLastNameChange} value={lastName} required type="text" name="last_name" id="last_name" className="form-control" />
      <label htmlFor="last name">Last name</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="address" onChange={handleAddressChange} value={address} required type="text" name="address" id="address" className="form-control" />
      <label htmlFor="address">Address</label>
    </div>

    <div className="form-floating mb-3">
      <input placeholder="Phone_Number" onChange={handlePhoneNumberChange} value={phoneNumber} required type="text" name="phone_number" id="phone_number" className="form-control" />
      <label htmlFor="phone_number">Phone Number</label>
    </div>

    <button className="btn btn-primary">Create</button>
  </form>
</div>
</div>
</div>
    )
}
export default CustomerForm;
