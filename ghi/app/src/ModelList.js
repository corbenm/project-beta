
function ModelList(props) {
    return (
        <>
<h1>Models</h1>
<table className="table table-hover table-bordered table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
        </tr>
    </thead>
    <tbody>
        {props.models.map(model => {
            return (
                <tr key={model.id}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td> <img src={model.picture_url} alt="" width="165" height="165" /> </td>
                </tr>
            );
        })}
    </tbody>
</table>
</>
    )
}

export default ModelList;
