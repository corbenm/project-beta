function SalespeopleList(props){
    return(
        <>
        <h1>Sales People</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>


                </tr>
            </thead>
            <tbody>
                {props.salespeople.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>

                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}
export default SalespeopleList;
