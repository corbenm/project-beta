import { useState, useEffect } from "react";

function ServiceHistory(props){
    const [autos, setAutos] = useState([]);
    let [filteredAppointments, setFilteredAppointments] = useState(props.appointments);
    const [search, setSearch] = useState('');

    // var initilized = false;
    if (filteredAppointments.length === 0){
        filteredAppointments = props.appointments;
    }

    async function getAutomobileVOs() {
        const response = await fetch('http://localhost:8080/api/autoVOs/');
        if (response.ok) {
            const { autos } = await response.json();
            setAutos(autos);
        } else {
            console.log("Issue with fetching autoVOs")
        }
    }
    useEffect(() => {
        getAutomobileVOs();
    }, []);

    function formatDate(timestamp){
        const formatted = new Date(timestamp);
        const simpleDate = formatted.toLocaleDateString("en-US", {
            month: "2-digit",
            day: "2-digit",
            year: "numeric",
        });
        return simpleDate;
    }

    const handleSearchChange = (event) => {
        const value = event.target.value;
        setSearch(value);
    }

    function formatTime(timestamp){
        const formatted = new Date(timestamp);
        const simpleTime = formatted.toLocaleTimeString("en-US",{
            hour12: true,
            hour: 'numeric',
            minute: 'numeric',
            timeZone: "UTC",
        });
        // console.log(simpleTime);
        return simpleTime;
    }

    function isVip(customerVIN){
        let status = "NO";
        for (let auto of autos){
         if (auto.vin == customerVIN){
             status = "YES";
             return status;
         }
        }
        return status;
     }

    function handleSearch(search) {
        let filtered = [];
        let lowerSearch = search.toLowerCase();
        for (let appointment of props.appointments) {
            let lowerVin = (appointment.vin).toLowerCase();
            if (lowerVin.includes(lowerSearch) ){
                filtered.push(appointment);
            }
        }
        // initilized = true;
        setFilteredAppointments(filtered);
     }


    return (
<>
<h1>Service History</h1>
<div className="input-group">
  <input type="text" onChange={handleSearchChange} value={search} className="form-control" placeholder="Search by Vin" aria-label="search" />
  <button onClick={() => handleSearch(search)} className="btn btn-outline-secondary" type="button">Search</button>
</div>


<table className="table table-hover table-bordered table-striped">
    <thead className="table-dark">
        <tr>
            <th>VIN</th>
            <th>Is Vip?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        {filteredAppointments.map(appointment => {
            return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ isVip(appointment.vin) }</td>
                    <td>{ appointment.customer }</td>
                    <td>{ formatDate(appointment.date_time) }</td>
                    <td>{ formatTime(appointment.date_time) }</td>
                    <td>{ (appointment.technician.first_name) + ' ' + (appointment.technician.last_name)}</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.status }</td>
                </tr>
            );
        })}
    </tbody>
</table>
</>
    )
}

export default ServiceHistory;
