import { useState } from "react";

function VehicleForm(props){
const [color, setColor] = useState('');
const [year, setYear] = useState('');
const [vin, setVin] = useState('');
const [model, setModel] = useState('');

const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
}
const handleYearChange = (event) => {
    const value = event.target.value;
    setYear(value);
}
const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
}
const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
}

async function handleSubmit(event) {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = model;

    const vehicleUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(vehicleUrl, fetchConfig);
    if (response.ok){
        const newVehicle = await response.json();
        console.log(newVehicle);
        setColor('');
        setYear('');
        setVin('');
        setModel('');
        props.getAutos();
    }
}

    return (
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Create a Vehicle</h1>
  <form onSubmit={handleSubmit} id="create-vehicle-form">
    <div className="form-floating mb-3">
      <input placeholder="Color" onChange={handleColorChange} value={color} required type="text" name="color" id="color" className="form-control" />
      <label htmlFor="color">Color</label>
    </div>
    <div className="form-floating mb-3">
        <input placeholder="Year" onChange={handleYearChange} value={year} required type="number" min={1900} max={2024} name="year" id="year" className="form-control" />
        <label htmlFor="year">Year</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Vin" onChange={handleVinChange} value={vin} required type="text" name="vin" id="vin" className="form-control" />
      <label htmlFor="vin">VIN</label>
    </div>
    <div className="mb-3">
      <select id="Model" onChange={handleModelChange} value={model} required name="model" className="form-select" >
      <option value="">Choose a model</option>
            {props.models.map(model => {
            return (
            <option key={model.id} value={model.id}> {model.name} </option>
            );
            })};
      </select>
    </div>
    <button className="btn btn-primary">Create</button>
  </form>
</div>
</div>
</div>
    )
}

export default VehicleForm;
