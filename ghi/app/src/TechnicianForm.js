import React, {useState} from 'react';

function TechnicianForm(props){
const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [employeeId, setEmployeeId] = useState('');

const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
}
const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
}
const handleIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
}

async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id =employeeId;

    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchconfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(technicianUrl, fetchconfig);
    if (response.ok) {
        const newTechnician = await response.json();
        console.log(newTechnician);
        setFirstName('');
        setLastName('');
        setEmployeeId('');
        props.getTechnicians();
    }
}

return(
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Add a Technician</h1>
  <form onSubmit={handleSubmit} id="create-technician-form">
    <div className="form-floating mb-3">
      <input placeholder="First name" onChange={handleFirstNameChange} value={firstName} required type="text" name="first_name" id="first_name" className="form-control" />
      <label htmlFor="first name">First name</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Last name" onChange={handleLastNameChange} value={lastName} required type="text" name="last_name" id="last_name" className="form-control" />
      <label htmlFor="last name">Last name</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Employee ID" onChange={handleIdChange} value={employeeId} required type="text" name="employee_id" id="employee_id" className="form-control" />
      <label htmlFor="employee_id">Employee ID</label>
    </div>
    <button className="btn btn-primary">Create</button>
  </form>
</div>
</div>
</div>
    )
}
export default TechnicianForm;
