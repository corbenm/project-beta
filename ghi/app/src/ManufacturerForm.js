import { useState } from "react";

function ManufactureForm(props){
const [manufacturer_name, setManufacturer_name] = useState('');

const handleManufacturerChange = (event) => {
  const value = event.target.value;
  setManufacturer_name(value);
}

async function handleSubmit(event) {
  event.preventDefault();

  const data = {};
  data.name = manufacturer_name;

  const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
  const fetchconfig = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(manufacturerUrl, fetchconfig);
  if (response.ok){
    const newManufacturer = await response.json();
    console.log(newManufacturer);
    setManufacturer_name('');
    props.getManufacturers();
  }
}

  return (
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Create a Manufacturer</h1>
  <form onSubmit={handleSubmit} id="create-manufacturer-form">
  <div className="form-floating mb-3">
      <input onChange={handleManufacturerChange} value={manufacturer_name} placeholder="Manufacturer_name" required type="text" name="manufacturer_name" id="manufacturer_name" className="form-control" />
      <label htmlFor="manufacturer_name">Manufacturer name</label>
    </div>
    <button className="btn btn-primary">Submit</button>
  </form>
</div>
</div>
</div>
    )
}

export default ManufactureForm;
