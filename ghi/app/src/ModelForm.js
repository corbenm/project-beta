import { useState } from "react";

function ModelForm(props){
const [modelName, setModelName] = useState('');
const [pictureUrl, setPictureUrl] = useState('');
const [manufacturer, setManufacturer] = useState('');


const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
}
const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
}
const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
}

async function handleSubmit(event) {
    event.preventDefault();

    const data = {};
    data.name = modelName;
    data.picture_url = pictureUrl;
    data.manufacturer_id = manufacturer;

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok){
        const newModel = await response.json();
        console.log(newModel);
        setModelName('');
        setPictureUrl('');
        setManufacturer('');
        props.getModels();
    }

}

    return (
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Create a Model</h1>
  <form onSubmit={handleSubmit} id="create-model-form">
  <div className="form-floating mb-3">
      <input  placeholder="Model_name" onChange={handleModelNameChange} value={modelName} required type="text" name="model_name" id="model_name" className="form-control" />
      <label htmlFor="model_name">Model name</label>
    </div>
    <div className="form-floating mb-3">
      <input  placeholder="Picture_url" onChange={handlePictureUrlChange} value={pictureUrl} required type="text" name="picture_url" id="picture_url" className="form-control" />
      <label htmlFor="picture_url">Picture Url</label>
    </div>
    <div className="mb-3">
      <select id="Manufacturers" onChange={handleManufacturerChange} value={manufacturer} required name="manufacturers" className="form-select" >
      <option value="">Choose a manufacturer</option>
            {props.manufacturers.map(manufacturer => {
            return (
            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
            );
            })};
      </select>
    </div>
    <button className="btn btn-primary">Create</button>
  </form>
</div>
</div>
</div>
    )
}
export default ModelForm;
