import { useState } from "react";
function SalespeopleForm(props){
const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [employee_id, setEmployee_id] = useState('');

const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
}
const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
}
const handleEmployee_idChange = (event) => {
    const value = event.target.value;
    setEmployee_id(value);
}

async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employee_id;

    const salesPersonURL = 'http://localhost:8090/api/salespeople/';
    const fetchconfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(salesPersonURL, fetchconfig);
    if (response.ok) {
        const newsalesPerson = await response.json();
        console.log(newsalesPerson);
        setFirstName('');
        setLastName('');
        setEmployee_id('');
        props.getSalespeople();
    }
}
    return(
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Add a Sales Person</h1>
  <form id="create-customer-form" onSubmit={handleSubmit}>
    <div className="form-floating mb-3">
      <input placeholder="First name" onChange={handleFirstNameChange} value={firstName} required type="text" name="first_name" id="first_name" className="form-control" />
      <label htmlFor="first name">First name</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="Last name" onChange={handleLastNameChange} value={lastName} required type="text" name="last_name" id="last_name" className="form-control" />
      <label htmlFor="last name">Last name</label>
    </div>
    <div className="form-floating mb-3">
      <input placeholder="employee_id" onChange={handleEmployee_idChange} value={employee_id} required type="text" name="employee_id" id="employee_id" className="form-control" />
      <label htmlFor="Employee_id">Employee_id</label>
    </div>
    <button className="btn btn-primary">Create</button>
  </form>
</div>
</div>
</div>
    )
}
export default SalespeopleForm;
