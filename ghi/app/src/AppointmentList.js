import { useState, useEffect } from "react";


function AppointmentList(props){

const [autos, setAutos] = useState([]);

    function formatDate(timestamp){
        const formatted = new Date(timestamp);
        const simpleDate = formatted.toLocaleDateString("en-US", {
            month: "2-digit",
            day: "2-digit",
            year: "numeric",
        });
        return simpleDate;
    }

    function formatTime(timestamp){
        const formatted = new Date(timestamp);
        const simpleTime = formatted.toLocaleTimeString("en-US",{
            hour12: true,
            hour: 'numeric',
            minute: 'numeric',
            timeZone: "UTC",
        });
        // console.log(simpleTime);
        return simpleTime;
    }

    function isVip(customerVIN){
        let status = "NO";
        for (let auto of autos){
         if (auto.vin == customerVIN){
             status = "YES";
             return status;
         }
        }
        return status;
     }

     async function handleFinish(AppointmentId) {
        const finishUrl = `http://localhost:8080/api/appointments/${AppointmentId}/finish`;
        const fetchConfig = {
            method: "PUT",
        }
        const response = await fetch(finishUrl, fetchConfig);
        if (response.ok) {
            props.getAppointments(AppointmentId);
            console.log("Set appointment status to finished")
        }
     }

     async function handleCancel(AppointmentId) {
        const finishUrl = `http://localhost:8080/api/appointments/${AppointmentId}/cancel`;
        const fetchConfig = {
            method: "PUT",
        }
        const response = await fetch(finishUrl, fetchConfig);
        if (response.ok) {
            props.getAppointments(AppointmentId);
            console.log("Set appointment status to cancelled")
        }
     }

    async function getAutomobileVOs() {
        const response = await fetch('http://localhost:8080/api/autoVOs/');
        if (response.ok) {
            const { autos } = await response.json();
            setAutos(autos);
        } else {
            console.log("Issue with fetching autoVOs")
        }
    }
    useEffect(() => {
        getAutomobileVOs();
    }, [])


    return(
        <>
        <h1>Appointments</h1>
        <table className="table table-hover table-bordered table-striped">
            <thead className="table-dark">
                <tr>
                    <th>VIN</th>
                    <th>Is Vip?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                    <th>Change status</th>
                </tr>
            </thead>
            <tbody>
                {props.appointments.map(appointment => {
                    if (appointment.status === 'PENDING'){
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ isVip(appointment.vin) }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ formatDate(appointment.date_time) }</td>
                            <td>{ formatTime(appointment.date_time) }</td>
                            <td>{ (appointment.technician.first_name) + ' ' + (appointment.technician.last_name)}</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                            <td>
                            <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                <button onClick={() => handleFinish(appointment.id)} className="btn btn-info me-md-2" type="button">FINISH</button>
                                <button onClick={() => handleCancel(appointment.id)} className="btn btn-warning" type="button">CANCEL</button>
                                </div>
                            </td>
                        </tr>
                    );
                    }})}
            </tbody>
        </table>
        </>
    );
}
export default AppointmentList
