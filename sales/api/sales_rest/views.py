from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from .models import SalesPerson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "href"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ['first_name', 'last_name', 'employee_id', 'id']


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number"]


class SalesEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "sales_person", "customer", "price"]
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }


from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(salesperson, encoder=SalesPersonEncoder, safe=False)
        except (ValueError, TypeError):
            return HttpResponseBadRequest("Invalid Data!")


@require_http_methods(["DELETE", "GET"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        salesperson = get_object_or_404(SalesPerson, id=id)
        return JsonResponse(salesperson, encoder=SalesPersonEncoder, safe=False)
    else:
        count, _ = SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except (ValueError, TypeError):
            return HttpResponseBadRequest("Invalid data")


@require_http_methods(["DELETE", "GET"])
def api_show_customer(request, id):
    if request.method == "GET":
        customer = get_object_or_404(Customer, id=id)
        return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
    else:
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SalesEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.create(**content)
            return JsonResponse(sale, encoder=SalesEncoder, safe=False)
        except (ValueError, TypeError):
            return HttpResponseBadRequest("Invalid Data!")


@require_http_methods(["DELETE", "GET"])
def api_show_sale(request, id):
    if request.method == "GET":
        sale = get_object_or_404(Sale, id=id)
        return JsonResponse(sale, encoder=SalesEncoder, safe=False)
    else:
        count, _ = Sale.objets.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
