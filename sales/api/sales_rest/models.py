from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50, unique=True)
    sold = models.BooleanField(default=False)
    href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.vin}"


class SalesPerson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.IntegerField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Sale(models.Model):
    sales_person = models.ForeignKey(SalesPerson, related_name="sp", on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, related_name="cust", on_delete=models.CASCADE)
    price = models.IntegerField()
    automobile = models.ForeignKey(AutomobileVO, related_name="auto", on_delete=models.CASCADE)

    def __str__(self):
        return f"sold: {self.customer} for: {self.price} auto: {self.automobile}"
